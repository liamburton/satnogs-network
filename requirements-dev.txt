# This is a generated file; DO NOT EDIT!
#
# Please edit 'setup.cfg' to add top-level extra dependencies and use
# './contrib/refresh-requirements.sh to regenerate this file
-r requirements.txt

coverage==6.4.1
distlib==0.3.4
execnet==1.9.0
factory-boy==3.2.1
Faker==13.15.0
filelock==3.7.1
iniconfig==1.1.1
mock==4.0.3
platformdirs==2.5.2
pluggy==1.0.0
py==1.11.0
pytest==7.1.2
pytest-cov==3.0.0
pytest-django==4.5.2
pytest-forked==1.4.0
pytest-xdist==2.5.0
toml==0.10.2
tomli==2.0.1
tox==3.24.5
virtualenv==20.15.1
